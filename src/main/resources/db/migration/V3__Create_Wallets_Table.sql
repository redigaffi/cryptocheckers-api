-- auto-generated definition
create table wallets
(
    id                 int auto_increment
        primary key,
    user_id            int                                      not null,
    currency           varchar(20)    default 'btc'             not null,
    deposit_address_id varchar(60)                              not null,
    deposit_address    varchar(60)                              not null,
    withdrawal_address varchar(255)                              null,
    amount             decimal(16, 8) default 0.00000000        not null,
    created_at         timestamp      default CURRENT_TIMESTAMP not null,
    updated_at         timestamp      default CURRENT_TIMESTAMP not null,
    constraint wallets_users_id_fk
        foreign key (user_id) references users (id)
);
