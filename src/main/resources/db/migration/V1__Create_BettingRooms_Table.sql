-- auto-generated definition
create table betting_rooms
(
    id       int auto_increment
        primary key,
    currency varchar(10) default 'BTC' not null,
    amount   decimal(16, 8)            not null,
    fees     decimal(16, 8)            not null,
    active   tinyint(1)  default 0     not null,
    created_at         timestamp      default CURRENT_TIMESTAMP not null,
    updated_at         timestamp      default CURRENT_TIMESTAMP not null
);

insert into betting_rooms (amount, fees, active) values (0.00009, 0.00002, true);