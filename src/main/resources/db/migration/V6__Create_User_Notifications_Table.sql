create table user_notifications
(
	id int auto_increment,
	user_id int not null,
	type varchar(90) not null,
	message varchar(90) null,
    created_at timestamp default current_timestamp null,
	updated_at timestamp default current_timestamp null,
	constraint user_notifications_pk
		primary key (id),
	constraint user_notifications_users_id_fk
		foreign key (user_id) references users (id)
);