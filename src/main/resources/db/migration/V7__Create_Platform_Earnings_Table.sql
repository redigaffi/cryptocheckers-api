create table platform_earnings
(
	id int auto_increment primary key,
	type varchar(90) not null,
	original_amount decimal(16, 8) not null,
	fees decimal(16, 8) not null,
	net_amount decimal(16, 8) not null,
    created_at timestamp default current_timestamp null,
	updated_at timestamp default current_timestamp null
);