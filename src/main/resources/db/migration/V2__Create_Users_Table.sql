create table users
(
    id             int auto_increment
        primary key,
    username       varchar(30)             not null,
    password       varchar(90)             not null,
    roles          int                     null,
    email          varchar(30)             not null,
    referred_by    int        default 1    not null,
    elo            int        default 1000 not null,
    public_profile tinyint(1) default 1    not null,
    betting_room   int       default 1    not null,
    active         bit(1)            default 0         not null,
    created_at         timestamp      default CURRENT_TIMESTAMP not null,
    updated_at         timestamp      default CURRENT_TIMESTAMP not null,
    constraint users_email_uindex
        unique (email),
    constraint users_username_uindex
        unique (username),
    constraint users_users_id_fk
        foreign key (referred_by) references users (id),
    constraint users_bettingroom_id_fk
        foreign key (betting_room) references betting_rooms (id)
);

insert into users (id, username, password, roles, email) VALUES (1, 'admin', '$2a$10$Ui6xmIrl4aKhfTB8vEKGmelgKHJZphfrvsSHSqRHHnklowT635PUG', 1, 'jordi.hoock@gmail.com');
insert into users (id, username, password, roles, email) VALUES (2, 'player1', '$2a$10$Ui6xmIrl4aKhfTB8vEKGmelgKHJZphfrvsSHSqRHHnklowT635PUG', 1, 'jordi.hoock1@gmail.com');
insert into users (id, username, password, roles, email) VALUES (3, 'player2', '$2a$10$Ui6xmIrl4aKhfTB8vEKGmelgKHJZphfrvsSHSqRHHnklowT635PUG', 1, 'jordi.hoock2@gmail.com');
insert into users (id, username, password, roles, email) VALUES (4, 'player3', '$2a$10$Ui6xmIrl4aKhfTB8vEKGmelgKHJZphfrvsSHSqRHHnklowT635PUG', 1, 'jordi.hoock3@gmail.com');
insert into users (id, username, password, roles, email) VALUES (5, 'player4', '$2a$10$Ui6xmIrl4aKhfTB8vEKGmelgKHJZphfrvsSHSqRHHnklowT635PUG', 1, 'jordi.hoock4@gmail.com');
/*
Password: imtosexxy99
 */