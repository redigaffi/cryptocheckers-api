create table wallet_transactions
(
	id int auto_increment,
	wallet_id int not null,
	amount DECIMAL(16,8) not null,
	type varchar(90) null,
	created_at timestamp default CURRENT_TIMESTAMP not null,
	updated_at timestamp default CURRENT_TIMESTAMP not null,
	coinbase_id varchar(255) null,
    network_hash varchar(255) null,
	constraint wallet_transactions_pk
		primary key (id),
	constraint wallet_transactions_wallets_id_fk
		foreign key (wallet_id) references wallets (id)
);

