create table matches
(
	id int auto_increment,
	player1 int not null,
	player2 int not null,
	status VARCHAR(90) default 'IN_PROGRESS' null,
	winner int null,
	looser int null,
	tie boolean default false null,
	created_at timestamp default current_timestamp null,
	updated_at timestamp default current_timestamp null,
	betting_room int not null,
	constraint matches_pk
		primary key (id),
	constraint matches_betting_rooms_id_fk
		foreign key (betting_room) references betting_rooms (id),
	constraint matches_users_id_fk
		foreign key (player1) references users (id),
	constraint matches_users_id_fk_2
		foreign key (player2) references users (id)
);
