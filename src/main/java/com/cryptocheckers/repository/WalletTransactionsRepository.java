package com.cryptocheckers.repository;

import com.cryptocheckers.entity.Wallet;
import com.cryptocheckers.entity.WalletTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WalletTransactionsRepository extends JpaRepository<WalletTransaction, Long> {

    Optional<List<WalletTransaction>> findAllByWallet(Wallet wallet);
    Optional<WalletTransaction> findByNetworkHash(String networkHash);

}
