package com.cryptocheckers.repository;

import com.cryptocheckers.entity.PlatformEarning;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlatformEarningRepository extends JpaRepository<PlatformEarning, Long> {

}
