package com.cryptocheckers.repository;

import com.cryptocheckers.entity.BettingRoom;
import com.cryptocheckers.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BettingRoomRepository extends JpaRepository<BettingRoom, Long> {
    List<BettingRoom> findByActive(Boolean active);
}
