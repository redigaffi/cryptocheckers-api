package com.cryptocheckers.repository;

import com.cryptocheckers.entity.EmailConfirmation;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EmailConfirmationRepository extends MongoRepository<EmailConfirmation, String>{
    EmailConfirmation findById(ObjectId objectId);
}
