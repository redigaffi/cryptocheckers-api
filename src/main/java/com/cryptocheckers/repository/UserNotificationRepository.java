package com.cryptocheckers.repository;

import com.cryptocheckers.entity.UserNotification;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserNotificationRepository extends JpaRepository<UserNotification, Long> {
}
