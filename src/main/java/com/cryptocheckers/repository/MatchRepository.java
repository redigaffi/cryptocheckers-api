package com.cryptocheckers.repository;

import com.cryptocheckers.entity.Match;
import com.cryptocheckers.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface MatchRepository extends JpaRepository<Match, Long> {

    @Query("select m from Match m WHERE m.status != 'IN_PROGRESS' and  (m.player1 = ?1 or m.player2 = ?1) ")
    Optional<List<Match>> findAllByUser(User user);
}
