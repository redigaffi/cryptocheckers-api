package com.cryptocheckers.eventlistener;

import com.cryptocheckers.entity.*;
import com.cryptocheckers.event.MatchEndedEvent;
import com.cryptocheckers.event.UpdateWalletFundsEvent;
import com.cryptocheckers.repository.PlatformEarningRepository;
import com.cryptocheckers.repository.WalletTransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import com.cryptocheckers.entity.WalletTransaction.TransactionType;
import java.math.BigDecimal;

@Component
public class MatchEndedEventListener implements ApplicationListener<MatchEndedEvent> {

    @Autowired
    private WalletTransactionsRepository walletTransactionsRepository;

    @Autowired
    private PlatformEarningRepository platformEarningRepository;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void onApplicationEvent(MatchEndedEvent event) {
        Match match = event.getMatch();
        User winner = match.getWinner();
        User looser = match.getLooser();

        BettingRoom bettingRoom = match.getBettingRoom();

        Wallet looserWallet = this.getWalletByPlayer(looser, bettingRoom);
        Wallet winnerWallet = this.getWalletByPlayer(winner, bettingRoom);

        // If tie rest fee to both
        if (event.getMatch().getTie()) {
            this.createAndSaveNewUserTransaction(looserWallet, TransactionType.MATCH_LOST, bettingRoom.getFees().negate());
            this.createAndSaveNewUserTransaction(winnerWallet, TransactionType.MATCH_WON, bettingRoom.getFees().negate());
            PlatformEarning platformEarning = new PlatformEarning();
            BigDecimal originalAmount = bettingRoom.getAmount().multiply(new BigDecimal(2));
            BigDecimal fees = bettingRoom.getFees().multiply(new BigDecimal(2));
            platformEarning.setOriginalAmount(originalAmount);
            platformEarning.setFees(fees);
            platformEarning.setNetAmount(fees);
            this.platformEarningRepository.save(platformEarning);
            return;
        }

        // Rest amount to looser
        this.createAndSaveNewUserTransaction(looserWallet, TransactionType.MATCH_LOST, bettingRoom.getAmount().negate());

        // Add amount to winner
        BigDecimal netWinningAmount = bettingRoom.getAmount().subtract(bettingRoom.getFees());
        this.createAndSaveNewUserTransaction(winnerWallet, TransactionType.MATCH_WON, netWinningAmount);

        // Add platform_earnings
        PlatformEarning platformEarning = new PlatformEarning();
        platformEarning.setOriginalAmount(bettingRoom.getAmount());
        platformEarning.setFees(bettingRoom.getFees());
        platformEarning.setNetAmount(bettingRoom.getFees());
        platformEarning.setType(PlatformEarning.Type.MATCH);
        this.platformEarningRepository.save(platformEarning);
    }

    private void createAndSaveNewUserTransaction(Wallet wallet, TransactionType transactionType, BigDecimal amount) {
        WalletTransaction walletTransaction = new WalletTransaction();
        walletTransaction.setWallet(wallet);
        walletTransaction.setType(transactionType);
        walletTransaction.setAmount(amount);
        this.walletTransactionsRepository.save(walletTransaction);
        this.applicationEventPublisher.publishEvent(new UpdateWalletFundsEvent(this, walletTransaction));
    }

    private Wallet getWalletByPlayer(User user, BettingRoom bettingRoom) {
        return user.getWallets()
                .stream()
                .filter(wallet -> wallet.getCurrency().equals(bettingRoom.getCurrency()))
                .findFirst()
                .get(); // Should always return a wallet
    }
}
