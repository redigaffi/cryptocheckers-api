package com.cryptocheckers.eventlistener;

import com.cryptocheckers.entity.Wallet;
import com.cryptocheckers.event.UpdateWalletFundsEvent;
import com.cryptocheckers.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;


import java.math.BigDecimal;

@Component
public class WalletTransactionEventListener implements ApplicationListener<UpdateWalletFundsEvent> {

    @Autowired
    private WalletRepository walletRepository;

    @Override
    public void onApplicationEvent(UpdateWalletFundsEvent event) {
        Wallet wallet = event.getWalletTransaction().getWallet();
        BigDecimal newAmount = wallet.getAmount().add(event.getWalletTransaction().getAmount());
        wallet.setAmount(newAmount);
        this.walletRepository.save(wallet);
    }
}
