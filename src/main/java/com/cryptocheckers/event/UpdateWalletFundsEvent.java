package com.cryptocheckers.event;

import com.cryptocheckers.entity.WalletTransaction;
import org.springframework.context.ApplicationEvent;

public class UpdateWalletFundsEvent extends ApplicationEvent {

    private WalletTransaction walletTransaction;

    public UpdateWalletFundsEvent(Object source, WalletTransaction walletTransaction) {
        super(source);
        this.walletTransaction = walletTransaction;
    }

    public WalletTransaction getWalletTransaction() {
        return walletTransaction;
    }
}
