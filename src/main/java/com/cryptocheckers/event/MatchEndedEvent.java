package com.cryptocheckers.event;

import com.cryptocheckers.entity.Match;
import org.springframework.context.ApplicationEvent;

public class MatchEndedEvent extends ApplicationEvent {

    private Match match;

    public MatchEndedEvent(Object source, Match match) {
        super(source);
        this.match = match;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }
}
