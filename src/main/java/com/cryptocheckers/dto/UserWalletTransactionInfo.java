package com.cryptocheckers.dto;

import com.cryptocheckers.entity.WalletTransaction;

import java.math.BigDecimal;

public class UserWalletTransactionInfo {

    private Long id;
    private String type;
    private BigDecimal amount;
    private String  createdAt;

    public UserWalletTransactionInfo(Long id, String type, BigDecimal amount, String createdAt) {
        this.id = id;
        this.type = type;
        this.amount = amount;
        this.createdAt = createdAt;
    }

    public static UserWalletTransactionInfo fromWalletTransactionEntity(WalletTransaction walletTransaction) {
        return new UserWalletTransactionInfo(walletTransaction.getId(), walletTransaction.getType().getType(), walletTransaction.getAmount(), walletTransaction.getCreatedAt().toString());
    }

    public Long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}
