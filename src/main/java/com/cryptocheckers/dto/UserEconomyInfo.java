package com.cryptocheckers.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;

@Getter
@NoArgsConstructor
public class UserEconomyInfo {
    public BigDecimal uplineComission = new BigDecimal(0);
    public BigDecimal totalDeposits = new BigDecimal(0);
    public BigDecimal totalWithdraws = new BigDecimal(0);
    public BigDecimal totalEarnings = new BigDecimal(0);
}
