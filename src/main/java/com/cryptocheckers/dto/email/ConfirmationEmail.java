package com.cryptocheckers.dto.email;

import java.util.HashMap;

public class ConfirmationEmail extends Email {

    public final static String TEMPLATE_NAME = "email_confirmation";
    private final static String SUBJECT      = "Please confirm your email";

    public ConfirmationEmail(String to, String username, String url) {
        super(to, ConfirmationEmail.SUBJECT);
        HashMap<String, String> contextData = new HashMap<>();
        contextData.put("username", username);
        contextData.put("confirmationLink", url);
        this.setContext(contextData);
    }

    @Override
    public String getTemplateName() {
        return ConfirmationEmail.TEMPLATE_NAME;
    }
}
