package com.cryptocheckers.dto.email;
import org.thymeleaf.context.Context;

import java.util.HashMap;

public abstract class Email {
    private String to;
    private String subject;
    private Context context;

    protected Email(String to, String subject) {
        this.to = to;
        this.subject = subject;
    }

    public String getTo() {
        return to;
    }

    public String getSubject() {
        return subject;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(HashMap<String, String> context) {
        this.context = this.buildContext(context);
    }

    private Context buildContext(HashMap<String, String> contextData) {
        Context context = new Context();
        contextData.forEach(context::setVariable);
        return context;
    }

    public abstract String getTemplateName();
}
