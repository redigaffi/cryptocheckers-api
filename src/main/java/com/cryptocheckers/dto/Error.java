package com.cryptocheckers.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.validation.FieldError;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Error {

    private String fieldName;
    private String message;
    private Integer code;

    public Error(String fieldName, String message) {
        this.fieldName = fieldName;
        this.setMessage(message);
    }

    public static Error fromFieldError(FieldError fieldError) {
        return new Error(fieldError.getField(), fieldError.getDefaultMessage());
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getFieldName() {
        return fieldName;
    }
}
