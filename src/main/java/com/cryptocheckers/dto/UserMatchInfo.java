package com.cryptocheckers.dto;

public class UserMatchInfo {
    public Integer matchesWon = 0;
    public Integer matchesTied = 0;
    public Integer matchesLost = 0;

    public UserMatchInfo() {}

    public Integer getMatchesWon() {
        return matchesWon;
    }

    public Integer getMatchesTied() {
        return matchesTied;
    }

    public Integer getMatchesLost() {
        return matchesLost;
    }
}
