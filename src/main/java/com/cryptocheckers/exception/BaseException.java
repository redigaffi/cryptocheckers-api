package com.cryptocheckers.exception;

import com.cryptocheckers.dto.Error;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor
public class BaseException extends Exception {
    private List<Error> errors;

    public BaseException(List<Error> errors) {
        this.errors = errors;
    }
}
