package com.cryptocheckers.exception;

import com.cryptocheckers.dto.Error;

import java.util.List;

public class RequestException extends BaseException {

    public RequestException(List<Error> error) {
        super(error);
    }
}
