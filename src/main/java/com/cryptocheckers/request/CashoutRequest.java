package com.cryptocheckers.request;

import java.math.BigDecimal;

public class CashoutRequest {

    private BigDecimal amount;
    private BigDecimal fees;
    private String currency;

    public CashoutRequest() {
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getFees() {
        return fees;
    }

    public void setFees(BigDecimal fees) {
        this.fees = fees;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
