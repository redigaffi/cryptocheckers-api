package com.cryptocheckers.request;

import javax.validation.constraints.NotNull;

public class MatchTerminatedRequest {
    private Long winner;
    private Long looser;

    public MatchTerminatedRequest() {
    }

    public Long getWinner() {
        return winner;
    }

    public void setWinner(Long winner) {
        this.winner = winner;
    }

    public Long getLooser() {
        return looser;
    }

    public void setLooser(Long looser) {
        this.looser = looser;
    }
}
