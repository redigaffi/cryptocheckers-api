package com.cryptocheckers.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class CreateUserRequest {
    @NotNull
    @Size(min = 5, max = 30)
    private String username;
    @NotNull
    @Size(min = 5, max = 30)
    private String password;
    @Email
    @NotNull
    private String email;
    private String referredBy;
    private Long bettingRoom;
    @NotNull
    private Map<String, String> cashoutAddresses;
}
