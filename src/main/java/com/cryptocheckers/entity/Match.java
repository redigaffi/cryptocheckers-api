package com.cryptocheckers.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "matches")
public class Match {

    public enum MatchStatus {
        IN_PROGRESS("IN_PROGRESS"),
        FINISHED("FINISHED");

        private String type;

        MatchStatus(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne
    @JoinColumn(name = "player1")
    private User player1;

    @ManyToOne
    @JoinColumn(name = "player2")
    private User player2;

    @Enumerated(EnumType.STRING)
    private MatchStatus status = MatchStatus.IN_PROGRESS;

    @ManyToOne
    @JoinColumn(name = "betting_room")
    private BettingRoom bettingRoom;

    @ManyToOne
    @JoinColumn(name = "winner")
    private User winner;

    @ManyToOne
    @JoinColumn(name = "looser")
    private User looser;

    private Boolean tie = false;

    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getPlayer1() {
        return player1;
    }

    public void setPlayer1(User player1) {
        this.player1 = player1;
    }

    public User getPlayer2() {
        return player2;
    }

    public void setPlayer2(User player2) {
        this.player2 = player2;
    }

    public MatchStatus getStatus() {
        return status;
    }

    public void setStatus(MatchStatus status) {
        this.status = status;
    }

    public BettingRoom getBettingRoom() {
        return bettingRoom;
    }

    public void setBettingRoom(BettingRoom bettingRoom) {
        this.bettingRoom = bettingRoom;
    }

    public User getWinner() {
        return winner;
    }

    public void setWinner(User winner) {
        this.winner = winner;
    }

    public User getLooser() {
        return looser;
    }

    public void setLooser(User looser) {
        this.looser = looser;
    }

    public Boolean getTie() {
        return tie;
    }

    public void setTie(Boolean tie) {
        this.tie = tie;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
