package com.cryptocheckers.entity;


import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    private static final int DEFAULT_ROLE = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;
    private String email;
    private Integer roles;
    private Integer elo = 1000;
    private Boolean active = false;

    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

    @ManyToOne
    @JoinColumn(name = "referred_by")
    private User referredBy;

    @ManyToOne
    @JoinColumn(name = "betting_room")
    private BettingRoom bettingRoom;

    @OneToMany(mappedBy = "referredBy")
    private List<User> referrals;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Wallet> wallets;

    @OneToMany(mappedBy = "user")
    private List<UserNotification> notifications;

    public Integer getElo() {
        return elo;
    }

    public void setElo(Integer elo) {
        this.elo = elo;
    }

    public User() {
        this.roles = DEFAULT_ROLE;
    }

    public BettingRoom getBettingRoom() {
        return bettingRoom;
    }

    public void setBettingRoom(BettingRoom bettingRoom) {
        this.bettingRoom = bettingRoom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getReferredBy() {
        return referredBy;
    }

    public void setReferredBy(User referredBy) {
        this.referredBy = referredBy;
    }

    public List<User> getReferrals() {
        return referrals;
    }

    public void setReferrals(List<User> referrals) {
        this.referrals = referrals;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Wallet> getWallets() {
        return wallets;
    }

    public void setWallets(List<Wallet> wallets) {
        this.wallets = wallets;
    }

    public List<UserNotification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<UserNotification> notifications) {
        this.notifications = notifications;
    }
    
    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}
