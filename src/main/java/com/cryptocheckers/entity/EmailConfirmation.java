package com.cryptocheckers.entity;

import org.bson.types.ObjectId;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Date;

public class EmailConfirmation {

    @Id
    private ObjectId id;

    private String email;

    @CreationTimestamp
    @Indexed(name = "createdAt", expireAfterSeconds = 7200)
    private Date createdAt;

    public EmailConfirmation() {
    }

    public EmailConfirmation(String email) {
        this.email = email;
    }

    public String getId() {
        return id.toHexString();
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
