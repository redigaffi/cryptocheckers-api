package com.cryptocheckers.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "wallet_transactions")
public class WalletTransaction {

    public enum TransactionType{
        DEPOSIT("DEPOSIT"),
        WITHDRAW("WITHDRAW"),
        UPLINE_COMMISSION("UPLINE_COMMISSION"),
        MATCH_WON("MATCH_WON"),
        MATCH_LOST("MATCH_LOST");

        private String type;

        TransactionType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // For deposits or Withdraws
    private String coinbaseId;

    private String networkHash;

    @Enumerated(EnumType.STRING)
    private TransactionType type;

    private BigDecimal amount = BigDecimal.valueOf(0D);

    @ManyToOne
    @JoinColumn(name = "wallet_id")
    private Wallet wallet;

    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCoinbaseId() {
        return coinbaseId;
    }

    public void setCoinbaseId(String coinbaseId) {
        this.coinbaseId = coinbaseId;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getNetworkHash() {
        return networkHash;
    }

    public void setNetworkHash(String networkHash) {
        this.networkHash = networkHash;
    }
}
