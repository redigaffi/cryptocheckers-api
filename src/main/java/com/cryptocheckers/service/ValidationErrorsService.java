package com.cryptocheckers.service;

import com.cryptocheckers.dto.Error;
import org.springframework.stereotype.Service;
import org.springframework.validation.FieldError;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ValidationErrorsService {

    public List<Error> fromFieldErrors(List<FieldError> errors) {
        return errors
                .stream()
                .map(Error::fromFieldError)
                .collect(Collectors.toList());

    }
}
