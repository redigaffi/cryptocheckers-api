package com.cryptocheckers.service;

import coinbase.api.v2.bean.CoinbaseAddress;
import coinbase.api.v2.exception.CoinbaseHttpException;
import com.cryptocheckers.constants.Currency;
import com.cryptocheckers.dto.Error;
import com.cryptocheckers.entity.User;
import com.cryptocheckers.entity.Wallet;
import com.cryptocheckers.exception.RequestException;
import com.cryptocheckers.repository.BettingRoomRepository;
import com.cryptocheckers.repository.UserRepository;
import com.cryptocheckers.repository.WalletRepository;
import com.cryptocheckers.request.CreateUserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.cryptocheckers.constants.Currency;

import java.util.*;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BettingRoomRepository bettingRoomRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CoinbaseService coinbaseService;

    @Autowired
    private WalletRepository walletRepository;

    public User createUser(CreateUserRequest createUserRequest, CoinbaseAddress coinbaseAddress) throws RequestException, CoinbaseHttpException {

        User user = new User();
        user.setUsername(createUserRequest.getUsername());
        user.setPassword(this.passwordEncoder.encode(createUserRequest.getPassword()));
        user.setEmail(createUserRequest.getEmail());
        user.setReferredBy(this.userRepository.findOne(1L));
        user.setBettingRoom(this.bettingRoomRepository.findOne(1L));

        if (createUserRequest.getBettingRoom() != null) {
            user.setBettingRoom(this.bettingRoomRepository.findOne(createUserRequest.getBettingRoom()));
        }

        if (createUserRequest.getReferredBy() != null) {
            Optional<User> referredBy = this.userRepository.findByUsername(createUserRequest.getReferredBy());
            referredBy.ifPresent(user::setReferredBy);
        }

        try {
            this.userRepository.saveAndFlush(user);
        } catch (DataIntegrityViolationException exception) {
            throw new RequestException(Arrays.asList(new Error("Username/Email", "Username or Email already exists")));
        }

        Wallet userWallet = new Wallet();
        userWallet.setUser(user);
        userWallet.setCurrency(Currency.fromFullCurrencyName(this.coinbaseService.getPrimaryAccount().getCurrency().getName()));
        userWallet.setWithdrawalAddress(createUserRequest.getCashoutAddresses().get(Currency.BTC.getType()));
        userWallet.setDepositAddressId(coinbaseAddress.getId());
        userWallet.setDepositAddress(coinbaseAddress.getAddress());
        this.walletRepository.save(userWallet);

        return user;
    }

    public void patchUser(User user, CreateUserRequest patchUserRequest) {

        if (patchUserRequest.getPassword() != null) {
            user.setPassword(this.passwordEncoder.encode(patchUserRequest.getPassword()));
        }

        if (patchUserRequest.getBettingRoom() != null) {
            user.setBettingRoom(this.bettingRoomRepository.findOne(patchUserRequest.getBettingRoom()));
        }

        if (patchUserRequest.getCashoutAddresses() != null && !patchUserRequest.getCashoutAddresses().isEmpty()) {

        }

    }
}
