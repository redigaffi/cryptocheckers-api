package com.cryptocheckers.service;

import com.cryptocheckers.dto.UserEconomyInfo;
import com.cryptocheckers.entity.User;

import com.cryptocheckers.entity.Wallet;
import com.cryptocheckers.entity.WalletTransaction;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EconomyUserInformationService {

    public UserEconomyInfo getEconomyInfoByUser(User user) {
        UserEconomyInfo userEconomyInfo = new UserEconomyInfo();

        List<Wallet> userWallets = user.getWallets();

        userWallets.forEach(wallet -> {
            List<WalletTransaction> walletTransactions = wallet.getWalletTransactions();
            walletTransactions.forEach(walletTransaction -> {
                if (walletTransaction.getType().equals(WalletTransaction.TransactionType.UPLINE_COMMISSION))
                    userEconomyInfo.uplineComission = userEconomyInfo.totalWithdraws.add(walletTransaction.getAmount());

                if (walletTransaction.getType().equals(WalletTransaction.TransactionType.DEPOSIT))
                    userEconomyInfo.totalDeposits = userEconomyInfo.totalWithdraws.add(walletTransaction.getAmount());


                if (walletTransaction.getType().equals(WalletTransaction.TransactionType.WITHDRAW))
                    userEconomyInfo.totalWithdraws = userEconomyInfo.totalWithdraws.add(walletTransaction.getAmount());

                if (walletTransaction.getType().equals(WalletTransaction.TransactionType.MATCH_WON))
                    userEconomyInfo.totalEarnings = userEconomyInfo.totalEarnings.add(walletTransaction.getAmount());

            });
        });

        return userEconomyInfo;
    }
}
