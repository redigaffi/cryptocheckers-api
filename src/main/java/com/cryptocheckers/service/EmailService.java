package com.cryptocheckers.service;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.*;
import com.cryptocheckers.dto.email.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class EmailService {

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private AmazonSimpleEmailService amazonSimpleEmailService;

    @Value("${email.from}")
    private String fromEmail;

    public void sendEmail(Email email) {
        SendEmailRequest request = new SendEmailRequest()
                .withDestination(
                        new Destination().withToAddresses(email.getTo()))
                .withMessage(new Message()
                        .withBody(new Body()
                                .withHtml(new Content()
                                        .withCharset("UTF-8").withData(this.buildHtmlEmail(email.getTemplateName(), email.getContext()))))
                        .withSubject(new Content()
                                .withCharset("UTF-8").withData(email.getSubject())))
                .withSource(this.fromEmail);

        this.amazonSimpleEmailService.sendEmail(request);
    }

    private String buildHtmlEmail(String template, Context context) {
        return this.templateEngine.process(template, context);
    }
}
