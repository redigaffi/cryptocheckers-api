package com.cryptocheckers.service;

import com.cryptocheckers.dto.UserMatchInfo;
import com.cryptocheckers.entity.Match;
import com.cryptocheckers.entity.User;
import com.cryptocheckers.repository.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MatchesUserInformationService {

    @Autowired
    private MatchRepository matchRepository;

    public UserMatchInfo getMatchInfoByUser(User user) {
        UserMatchInfo userMatchInfo = new UserMatchInfo();
        Optional<List<Match>> optionalMatches = this.matchRepository.findAllByUser(user);

        optionalMatches.ifPresent(matches -> {
            matches.forEach(match -> {
                if (match.getTie()) {
                    userMatchInfo.matchesTied++;
                    return;
                }

                if (match.getWinner().getId().equals(user.getId())) userMatchInfo.matchesWon++;
                if (match.getLooser().getId().equals(user.getId())) userMatchInfo.matchesLost++;
            });
        });

        return userMatchInfo;
    }
}
