package com.cryptocheckers.service;

import coinbase.api.v2.bean.CoinbaseAccount;
import coinbase.api.v2.bean.CoinbaseAddress;
import coinbase.api.v2.bean.CoinbaseAmount;
import coinbase.api.v2.bean.CoinbaseTransaction;
import coinbase.api.v2.bean.operation.CoinbaseSendMoney;
import coinbase.api.v2.bean.response.CoinbaseNotification;
import coinbase.api.v2.exception.CoinbaseHttpException;
import coinbase.api.v2.service.CoinbaseAccountService;
import coinbase.api.v2.service.CoinbaseAddressService;
import coinbase.api.v2.service.CoinbaseTransactionService;
import coinbase.api.v2.service.auth.CoinbaseAuthenticationBearer;
import com.cryptocheckers.constants.Currency;
import com.cryptocheckers.dto.Error;
import com.cryptocheckers.entity.User;
import com.cryptocheckers.entity.Wallet;
import com.cryptocheckers.entity.WalletTransaction;
import com.cryptocheckers.event.UpdateWalletFundsEvent;
import com.cryptocheckers.exception.RequestException;
import com.cryptocheckers.repository.WalletRepository;
import com.cryptocheckers.repository.WalletTransactionsRepository;
import com.cryptocheckers.request.CashoutRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CoinbaseService {


    @Autowired
    private CoinbaseAuthenticationBearer coinbaseAuth;

    @Autowired
    private CoinbaseAccountService coinbaseAccountService;

    @Autowired
    private CoinbaseAddressService coinbaseAddressService;

    @Autowired
    private CoinbaseTransactionService coinbaseTransactionService;

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private WalletTransactionsRepository walletTransactionsRepository;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    private final Logger logger = LoggerFactory.getLogger(CoinbaseService.class);

    public CoinbaseAccount getPrimaryAccount() throws CoinbaseHttpException {
        List<CoinbaseAccount> accounts = this.coinbaseAccountService.list(this.coinbaseAuth).getData();

        return accounts
                .stream()
                .filter(CoinbaseAccount::getPrimary)
                .collect(Collectors.toList()).get(0);

    }

    public void sendFunds(CashoutRequest cashoutRequest, Wallet userWallet) throws RequestException {
        List<Error> errors = new ArrayList<>();

        BigDecimal total = cashoutRequest.getAmount().add(cashoutRequest.getFees());

        CoinbaseSendMoney coinbaseSendMoney = new CoinbaseSendMoney();
        coinbaseSendMoney.setAmount(total);
        coinbaseSendMoney.setCurrency(cashoutRequest.getCurrency());
        coinbaseSendMoney.setDescription("Enjoy your withdraw from cryptocheckers.io, thanks for playing!");
        coinbaseSendMoney.setTo(userWallet.getWithdrawalAddress());

        try {
            CoinbaseTransaction coinbaseTransaction = this.coinbaseTransactionService.send(this.coinbaseAuth, this.getPrimaryAccount().getId(), coinbaseSendMoney);

            if ("completed".equals(coinbaseTransaction.getStatus()) || "pending".equals(coinbaseTransaction.getStatus())) {

                WalletTransaction walletTransaction = new WalletTransaction();
                walletTransaction.setCoinbaseId(coinbaseTransaction.getId());
                walletTransaction.setNetworkHash(coinbaseTransaction.getNetwork().getHash());
                walletTransaction.setType(WalletTransaction.TransactionType.WITHDRAW);
                BigDecimal toRestAmount = total.negate();
                walletTransaction.setAmount(toRestAmount);
                walletTransaction.setWallet(userWallet);
                this.walletTransactionsRepository.save(walletTransaction);
                this.applicationEventPublisher.publishEvent(new UpdateWalletFundsEvent(this, walletTransaction));

            } else {
                //@todo: failed for some reason...
                errors.add(new Error("", "Something failed during the cashout"));
                throw new RequestException(errors);
            }
        } catch (Exception ex) {
            errors.add(new Error("", "Something failed during the cashout"));
            throw new RequestException(errors);
        }
    }

    public CoinbaseAddress createNewAddressForUser() throws CoinbaseHttpException {
        CoinbaseAccount primaryAccount = this.getPrimaryAccount();
        return this.coinbaseAddressService.create(this.coinbaseAuth, primaryAccount.getId());
    }

    public void creditUserDeposit(CoinbaseNotification coinbaseNotification) {
        String depositAddressId = coinbaseNotification.getData().getId();

        Optional<Wallet> walletOptional = this.walletRepository.findByDepositAddressId(depositAddressId);

        if (walletOptional.isPresent()) {

            Optional<WalletTransaction> walletTransactionOptional =
                    this.walletTransactionsRepository.findByNetworkHash(coinbaseNotification.getCoinbaseAdditionalData().getHash());

            if (walletTransactionOptional.isPresent()) {
                logger.error("Webhook already processed, hash={}, depositAddressId={}, amount={}",
                        coinbaseNotification.getCoinbaseAdditionalData().getHash(),
                        depositAddressId,
                        coinbaseNotification.getCoinbaseAdditionalData().getAmount().getAmount());
                return;
            }

            CoinbaseAmount depositAmount = coinbaseNotification.getCoinbaseAdditionalData().getAmount();

            Wallet wallet = walletOptional.get();

            /*BigDecimal walletAmount = wallet.getAmount();
            wallet.setAmount(walletAmount.add(depositAmount.getAmount()));
            this.walletRepository.save(wallet);*/

            WalletTransaction walletTransaction = new WalletTransaction();
            walletTransaction.setCoinbaseId(coinbaseNotification.getCoinbaseAdditionalData().getTransaction().getId());
            walletTransaction.setType(WalletTransaction.TransactionType.DEPOSIT);
            walletTransaction.setAmount(depositAmount.getAmount());
            walletTransaction.setWallet(wallet);
            walletTransaction.setNetworkHash(coinbaseNotification.getCoinbaseAdditionalData().getHash());
            this.walletTransactionsRepository.save(walletTransaction);
            this.applicationEventPublisher.publishEvent(new UpdateWalletFundsEvent(this, walletTransaction));
        }
    }
}
