package com.cryptocheckers.service;

import coinbase.api.v2.bean.CoinbaseAmount;
import coinbase.api.v2.exception.CoinbaseHttpException;
import coinbase.api.v2.service.data.CoinbasePricesService;
import com.cryptocheckers.entity.BettingRoom;
import com.cryptocheckers.repository.BettingRoomRepository;
import com.cryptocheckers.response.BettingRoomsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class BettingRoomExchangeRateService {

    @Autowired
    private CoinbasePricesService coinbasePricesService;

    @Autowired
    private BettingRoomRepository bettingRoomRepository;


    public List<BettingRoomsResponse> bettingRoomsWithExchangeRates() throws CoinbaseHttpException {
        List<BettingRoom> bettingRooms = this.bettingRoomRepository.findByActive(true);

        List<BettingRoomsResponse> bettingRoomsResponse = new ArrayList<>();
        for (BettingRoom bettingRoom : bettingRooms) {
            bettingRoomsResponse.add(this.bettingRoomWithExchangeRates(bettingRoom));
        }

        return  bettingRoomsResponse;
    }

    public BettingRoomsResponse bettingRoomWithExchangeRates(BettingRoom bettingRoom) throws CoinbaseHttpException {
        String currency = bettingRoom.getCurrency().toString();
        CoinbaseAmount amount = this.coinbasePricesService.getBuyPrice(currency, "USD");
        BigDecimal bettingRoomUsdCost = amount.getAmount().multiply(bettingRoom.getAmount());
        BigDecimal bettingRoomFeeUsdCost = amount.getAmount().multiply(bettingRoom.getFees());

        return new BettingRoomsResponse(bettingRoom.getId(),
                currency,
                bettingRoom.getAmount(),
                bettingRoom.getFees(),
                bettingRoomUsdCost,
                bettingRoomFeeUsdCost,
                bettingRoom.getUsersInRoom().size());
    }
}
