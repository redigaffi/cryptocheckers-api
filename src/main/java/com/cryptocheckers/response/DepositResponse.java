package com.cryptocheckers.response;

import com.cryptocheckers.constants.Currency;
import com.cryptocheckers.entity.Wallet;

public class DepositResponse {
    private Currency currency;
    private String address;


    public static DepositResponse fromWallet(Wallet wallet) {
        DepositResponse depositResponse = new DepositResponse();

        depositResponse.setAddress(wallet.getDepositAddress());
        depositResponse.setCurrency(wallet.getCurrency());

        return depositResponse;
    }

    public DepositResponse() {
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
