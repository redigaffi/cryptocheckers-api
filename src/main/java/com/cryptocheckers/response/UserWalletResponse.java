package com.cryptocheckers.response;

import com.cryptocheckers.constants.Currency;
import com.cryptocheckers.entity.Wallet;

public class UserWalletResponse {
    private Long id;
    private Currency currency;

    public UserWalletResponse(Long id, Currency currency) {
        this.id = id;
        this.currency = currency;
    }

    public static UserWalletResponse fromWalletEntity(Wallet wallet) {
        return new UserWalletResponse(wallet.getId(), wallet.getCurrency());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
