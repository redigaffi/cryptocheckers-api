package com.cryptocheckers.response;

public class CanUserPlayMatch {
    private Boolean canPlay;
    private String message = "";

    public CanUserPlayMatch(Boolean canPlay) {
        this.canPlay = canPlay;
    }

    public CanUserPlayMatch(Boolean canPlay, String message) {
        this.canPlay = canPlay;
        this.message = message;
    }

    public Boolean getCanPlay() {
        return canPlay;
    }

    public String getMessage() {
        return message;
    }
}
