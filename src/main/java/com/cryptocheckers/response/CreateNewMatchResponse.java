package com.cryptocheckers.response;

public class CreateNewMatchResponse {

    private Long roomId;

    public CreateNewMatchResponse() {}

    public CreateNewMatchResponse(Long roomId) {
        this.roomId = roomId;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }
}
