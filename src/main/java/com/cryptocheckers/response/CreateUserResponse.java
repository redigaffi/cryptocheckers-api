package com.cryptocheckers.response;

import com.cryptocheckers.entity.User;

public class CreateUserResponse {
    private Long id;
    private String username;
    private String email;
    private Long referredBy;
    private Long bettingRoom;

    public CreateUserResponse() {}

    private CreateUserResponse(Long id, String username, String email, Long referredBy, Long bettingRoom) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.referredBy = referredBy;
        this.bettingRoom = bettingRoom;
    }

    public static CreateUserResponse createUserResponseFromUserEntity(User user) {
        return new CreateUserResponse(user.getId(), user.getUsername(), user.getEmail(), user.getReferredBy().getId(), user.getBettingRoom().getId());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getReferredBy() {
        return referredBy;
    }

    public void setReferredBy(Long referredBy) {
        this.referredBy = referredBy;
    }

    public Long getBettingRoom() {
        return bettingRoom;
    }

    public void setBettingRoom(Long bettingRoom) {
        this.bettingRoom = bettingRoom;
    }
}
