package com.cryptocheckers.response;


import com.cryptocheckers.dto.UserEconomyInfo;
import com.cryptocheckers.dto.UserMatchInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class UserProfileResponse {
    private String username;
    private UserMatchInfo userMatchInfo;
    private UserEconomyInfo userEconomyInfo;
    private String registrationDate;
    private Integer numberOfReferrals;
    private BettingRoomsResponse bettingRoom;

    public UserProfileResponse(String username, UserMatchInfo userMatchInfo, UserEconomyInfo userEconomyInfo, String registrationDate, Integer numberOfReferrals, BettingRoomsResponse bettingRoom) {
        this.username = username;
        this.userMatchInfo = userMatchInfo;
        this.userEconomyInfo = userEconomyInfo;
        this.registrationDate = registrationDate;
        this.numberOfReferrals = numberOfReferrals;
        this.bettingRoom = bettingRoom;
    }
}
