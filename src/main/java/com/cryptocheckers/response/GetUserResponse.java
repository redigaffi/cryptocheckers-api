package com.cryptocheckers.response;

import com.cryptocheckers.entity.BettingRoom;
import com.cryptocheckers.entity.User;

public class GetUserResponse {
    private Long id;
    private String username;
    private Integer elo;
    private Long bettingRoomId;
    private String email;

    public static GetUserResponse fromUserEntity(User user) {
        return new GetUserResponse(
                user.getId(),
                user.getUsername(),
                user.getElo(),
                user.getBettingRoom().getId(),
                user.getEmail()
        );
    }

    public GetUserResponse(Long id, String username, Integer elo, Long bettingRoom, String email) {
        this.id = id;
        this.username = username;
        this.elo = elo;
        this.bettingRoomId = bettingRoom;
        this.email = email;
    }

    public GetUserResponse() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getElo() {
        return elo;
    }

    public void setElo(Integer elo) {
        this.elo = elo;
    }

    public Long getBettingRoom() {
        return bettingRoomId;
    }

    public void setBettingRoom(Long bettingRoomId) {
        this.bettingRoomId = bettingRoomId;
    }
}
