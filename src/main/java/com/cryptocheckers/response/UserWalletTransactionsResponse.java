package com.cryptocheckers.response;

import com.cryptocheckers.constants.Currency;
import com.cryptocheckers.dto.UserWalletTransactionInfo;
import com.fasterxml.jackson.annotation.JsonValue;


import java.util.HashMap;
import java.util.List;

public class UserWalletTransactionsResponse {


    private HashMap<Currency, List<UserWalletTransactionInfo>> walletTransactions = new HashMap<>();

    public UserWalletTransactionsResponse() {}

    @JsonValue
    public HashMap<Currency, List<UserWalletTransactionInfo>> getWalletTransactions() {
        return walletTransactions;
    }

    public void addWalletTransactions(Currency key, List<UserWalletTransactionInfo> value) {
        this.walletTransactions.put(key, value);
    }


}
