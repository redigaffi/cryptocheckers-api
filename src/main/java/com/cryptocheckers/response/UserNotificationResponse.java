package com.cryptocheckers.response;

import com.cryptocheckers.entity.UserNotification;

public class UserNotificationResponse {

    private String type;
    private String message;

    public UserNotificationResponse(String type, String message) {
        this.type = type;
        this.message = message;
    }

    public static UserNotificationResponse fromUserNotificationEntity(UserNotification userNotification) {
            return new UserNotificationResponse(userNotification.getType(), userNotification.getMessage());
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
