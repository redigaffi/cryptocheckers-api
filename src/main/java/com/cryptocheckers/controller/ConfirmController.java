package com.cryptocheckers.controller;

import java.util.Date;
import java.util.Optional;

import com.cryptocheckers.config.JwtConfigBag;
import com.cryptocheckers.entity.EmailConfirmation;
import com.cryptocheckers.entity.User;
import com.cryptocheckers.exception.BaseException;
import com.cryptocheckers.repository.EmailConfirmationRepository;
import com.cryptocheckers.repository.UserRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/confirm")
public class ConfirmController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmailConfirmationRepository emailConfirmationRepository;

    @GetMapping("/new-account/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void confirmNewAccount(@PathVariable("id") String id, HttpServletResponse response) throws Exception {
        var emailConfirmation = this.emailConfirmationRepository.findById(new ObjectId(id));
        var userOptional = this.userRepository.findByEmail(emailConfirmation.getEmail());
        var user = userOptional.orElseThrow(BaseException::new);

        user.setActive(true);
        this.userRepository.save(user);
        this.emailConfirmationRepository.delete(emailConfirmation);

        // Auto login
        String token = Jwts.builder().setIssuedAt(new Date()).setIssuer(JwtConfigBag.ISSUER_INFO)
                .setSubject(user.getUsername())
                .setExpiration(new Date(System.currentTimeMillis() + JwtConfigBag.TOKEN_EXPIRATION_KEY))
                .signWith(SignatureAlgorithm.HS512, JwtConfigBag.SECRET_KEY).compact();

        response.addHeader(JwtConfigBag.HEADER_AUTHORIZATION_KEY, JwtConfigBag.TOKEN_BEARER_PREFIX + token);
    }
}
