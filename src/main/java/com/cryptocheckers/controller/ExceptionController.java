package com.cryptocheckers.controller;

import com.cryptocheckers.exception.RequestException;
import com.cryptocheckers.response.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@RestController
public class ExceptionController  {

    @ExceptionHandler(RequestException.class)
    public final ResponseEntity<ErrorResponse> handleAllExceptions(RequestException ex, WebRequest request) {
        return new ResponseEntity<>(new ErrorResponse(ex.getErrors()), HttpStatus.BAD_REQUEST);
    }
}
