package com.cryptocheckers.controller;

import coinbase.api.v2.bean.response.CoinbaseNotification;
import coinbase.api.v2.exception.CoinbaseHttpException;
import com.cryptocheckers.service.CoinbaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.web.util.matcher.IpAddressMatcher;

import javax.servlet.http.HttpServletRequest;

@RestController
public class WebhookController {

    @Autowired
    private CoinbaseService coinbaseService;
    private final Logger logger = LoggerFactory.getLogger(WebhookController.class);

    @PostMapping("/webhook")
    public ResponseEntity<String> webhook(HttpServletRequest request, @RequestBody CoinbaseNotification coinbaseNotification) throws CoinbaseHttpException {
        logger.info("Webhook endpoint called");
        if (!this.isCoinbaseOrigin(request.getRemoteAddr())) {
            logger.info("Webhook endpoint called: But IP doesn't comes from Coinbase trusted ip.");
            return ResponseEntity.badRequest().body("You can't do that.");
        }

        if ("wallet:addresses:new-payment".equals(coinbaseNotification.getType())) {
            logger.info("Webhook endpoint called: New deposit, amount={}, depositAddressId={}, hash={}",
                    coinbaseNotification.getCoinbaseAdditionalData().getAmount().getAmount(),
                    coinbaseNotification.getData().getId(),
                    coinbaseNotification.getCoinbaseAdditionalData().getHash());

            this.coinbaseService.creditUserDeposit(coinbaseNotification);
        }

        return ResponseEntity.ok("");
    }

    private boolean isCoinbaseOrigin(String ip) {
        // Coinbase subnet
        IpAddressMatcher ipAddressMatcher = new IpAddressMatcher("54.175.255.192/27");
        return ipAddressMatcher.matches(ip);
    }
}
