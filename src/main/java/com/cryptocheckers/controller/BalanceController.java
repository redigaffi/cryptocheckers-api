package com.cryptocheckers.controller;

import coinbase.api.v2.exception.CoinbaseHttpException;
import com.cryptocheckers.constants.Currency;
import com.cryptocheckers.dto.Error;
import com.cryptocheckers.entity.User;
import com.cryptocheckers.entity.Wallet;
import com.cryptocheckers.exception.RequestException;
import com.cryptocheckers.repository.UserRepository;
import com.cryptocheckers.request.CashoutRequest;
import com.cryptocheckers.response.DepositResponse;
import com.cryptocheckers.service.CoinbaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class BalanceController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CoinbaseService coinbaseService;

    @GetMapping("/myBalance")
    public HashMap<Currency, String> myBalance(HttpServletRequest request) {

        HashMap<Currency, String> result = new HashMap<>();
        Optional<User> userOptional = this.userRepository.findByUsername(request.getUserPrincipal().getName());

        userOptional.ifPresent(user -> {
            List<Wallet> userWallets = user.getWallets();
            userWallets.forEach(userWallet -> {
                result.put(userWallet.getCurrency(), userWallet.getAmount().toPlainString());
            });
        });

        return result;
    }

    @GetMapping("/deposit")
    @ResponseBody()
    public List<DepositResponse> getDepositAddresses(HttpServletRequest request) {
        List<DepositResponse> depositResponse = new ArrayList<>();
        
        Optional<User> userOptional = this.userRepository.findByUsername(request.getUserPrincipal().getName());
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            depositResponse = user.getWallets()
                    .stream()
                    .map(DepositResponse::fromWallet)
                    .collect(Collectors.toList());
        }

        return depositResponse;
    }


    @PostMapping("/cashout")
    @ResponseBody()
    public void cashout(HttpServletRequest request, @RequestBody @Valid CashoutRequest cashoutRequest) throws CoinbaseHttpException, RequestException {
        Optional<User> userOptional = this.userRepository.findByUsername(request.getUserPrincipal().getName());

        if (!userOptional.isPresent()) {
            return;
        }

        User user = userOptional.get();

        List<Error> errors = new ArrayList<>();
        if (cashoutRequest.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            errors.add(
                    new Error("amount", "Amount cant be less/equal than zero")
            );
        }

        if (cashoutRequest.getFees().compareTo(BigDecimal.ZERO) <= 0) {
            errors.add(
                    new Error("fees", "Fees cant be less/equal than zero")
            );
        }

        List<Wallet> userWallet = user.getWallets()
                .stream()
                .filter(wallet -> wallet.getCurrency().getType().equals(cashoutRequest.getCurrency()))
                .collect(Collectors.toList());

        if (userWallet.isEmpty()) {
            errors.add(
                    new Error("currency", "Wrong currency code")
            );
        }


        BigDecimal totalWithdraw = cashoutRequest.getAmount().add(cashoutRequest.getFees());
        if (userWallet.get(0).getAmount().compareTo(totalWithdraw) < 0) {
            errors.add(
                    new Error("amount", "Not enough balance to cover the withdraw")
            );
        }

        if (userWallet.get(0).getWithdrawalAddress() == null) {
            errors.add(
                    new Error("address", "Withdrawal address is not configured, please configure a wallet in your profile settings")
            );
        }


        if (!errors.isEmpty()) {
            throw new RequestException(errors);
        }

        this.coinbaseService.sendFunds(cashoutRequest, userWallet.get(0));
    }
}
