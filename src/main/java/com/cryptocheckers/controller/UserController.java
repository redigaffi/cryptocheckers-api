package com.cryptocheckers.controller;

import coinbase.api.v2.exception.CoinbaseHttpException;
import com.cryptocheckers.constants.Currency;
import com.cryptocheckers.dto.UserEconomyInfo;
import com.cryptocheckers.dto.UserMatchInfo;
import com.cryptocheckers.dto.UserWalletTransactionInfo;
import com.cryptocheckers.dto.email.ConfirmationEmail;
import com.cryptocheckers.entity.*;
import com.cryptocheckers.exception.BaseException;
import com.cryptocheckers.exception.RequestException;
import com.cryptocheckers.repository.*;
import com.cryptocheckers.request.CreateUserRequest;
import com.cryptocheckers.response.*;
import com.cryptocheckers.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
public class UserController {

    @Autowired
    private ValidationErrorsService validationErrorsService;

    @Autowired
    private UserService userService;
    
    @Autowired
    private CoinbaseService coinbaseService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BettingRoomExchangeRateService bettingRoomExchangeRateService;

    @Autowired
    private MatchesUserInformationService matchesUserInformationService;

    @Autowired
    private EconomyUserInformationService economyUserInformationService;

    @Autowired
    private UserNotificationRepository userNotificationRepository;

    @Autowired
    private EmailConfirmationRepository emailConfirmationRepository;
    
    @Autowired
    private EmailService emailService;

    @Value("${cryptocheckers.domain}")
    protected String baseUrl;

    @GetMapping("/user")
    public GetUserResponse getUser(HttpServletRequest request) {
        var userOptional = this.userRepository.findByUsername(request.getUserPrincipal().getName());
        // @todo: throw error if user not found
        return GetUserResponse.fromUserEntity(userOptional.get());
    }

    @PatchMapping("/user")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void patchUser(HttpServletRequest request, @RequestBody CreateUserRequest patchUserRequest) throws BaseException {
        var user = this.userRepository.findByUsername(request.getUserPrincipal().getName())
                .orElseThrow(BaseException::new);

        this.userService.patchUser(user, patchUserRequest);
    }


    @PostMapping("/register")
    @ResponseBody()
    @ResponseStatus(HttpStatus.CREATED)
    public CreateUserResponse register(HttpServletResponse httpServletResponse, @RequestBody @Valid  CreateUserRequest createUserRequest, BindingResult bindingResult) throws CoinbaseHttpException, RequestException {
        if (bindingResult.hasErrors()) {
            httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
            throw new RequestException(this.validationErrorsService.fromFieldErrors(bindingResult.getFieldErrors()));
        }

        var coinbaseAddress =  this.coinbaseService.createNewAddressForUser();
        var user = this.userService.createUser(createUserRequest, coinbaseAddress);

        EmailConfirmation emailConfirmation = new EmailConfirmation();
        emailConfirmation.setEmail(user.getEmail());
        this.emailConfirmationRepository.save(emailConfirmation);
        
        String confirmationLink = this.baseUrl.concat("/confirm/new-account/"+emailConfirmation.getId());
        ConfirmationEmail confirmationEmail = new ConfirmationEmail(user.getEmail(), user.getUsername(), confirmationLink);
        this.emailService.sendEmail(confirmationEmail);

        var userNotification = new UserNotification();
        userNotification.setUser(user);
        userNotification.setMessage("Welcome "+user.getUsername()+", please confirm your email address.");
        userNotification.setType("register");
        userNotificationRepository.saveAndFlush(userNotification);

        return CreateUserResponse.createUserResponseFromUserEntity(user);
    }

    @GetMapping("/bettingRooms")
    public List<BettingRoomsResponse> bettingRooms() throws CoinbaseHttpException {
        return this.bettingRoomExchangeRateService.bettingRoomsWithExchangeRates();
    }

    @GetMapping("/profile")
    @ResponseBody()
    public UserProfileResponse userProfileResponse(HttpServletRequest request) throws CoinbaseHttpException {

        UserProfileResponse userProfileResponse = new UserProfileResponse();
        Optional<User> userOptional = this.userRepository.findByUsername(request.getUserPrincipal().getName());

        if (userOptional.isPresent()) {

            User user = userOptional.get();
            UserMatchInfo userMatchInfo = this.matchesUserInformationService.getMatchInfoByUser(user);
            UserEconomyInfo userEconomyInfo = this.economyUserInformationService.getEconomyInfoByUser(user);

            userProfileResponse = new UserProfileResponse(user.getUsername(),
                    userMatchInfo,
                    userEconomyInfo,
                    user.getCreatedAt().toString(),
                    user.getReferrals().size(),
                    this.bettingRoomExchangeRateService.bettingRoomWithExchangeRates(user.getBettingRoom()));
        }

        return userProfileResponse;
    }


    @GetMapping("/notifications")
    @ResponseBody()
    public List<UserNotificationResponse> userNotifications(HttpServletRequest request) {
        List<UserNotificationResponse> response = new ArrayList<>();

        Optional<User> userOptional = this.userRepository.findByUsername(request.getUserPrincipal().getName());
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            var userNotifications = user.getNotifications();

            response = userNotifications
                            .stream()
                            .map(UserNotificationResponse::fromUserNotificationEntity)
                            .collect(Collectors.toList());

            userNotifications
                    .forEach(userNotification -> this.userNotificationRepository.delete(userNotification.getId()));
        }

        return response;
    }

    @GetMapping("/myTransactions")
    @ResponseBody()
    public UserWalletTransactionsResponse myWallets(HttpServletRequest request) {
        UserWalletTransactionsResponse userWalletTransactionsResponse = new UserWalletTransactionsResponse();

        Optional<User> userOptional = this.userRepository.findByUsername(request.getUserPrincipal().getName());
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            List<Wallet> userWallets = user.getWallets();

            userWallets
                    .forEach(userWallet -> {
                        var userWalletTransactionInfo = new ArrayList<UserWalletTransactionInfo>();
                        userWallet.getWalletTransactions().forEach(walletTransaction -> userWalletTransactionInfo.add(UserWalletTransactionInfo.fromWalletTransactionEntity(walletTransaction)));
                        userWalletTransactionsResponse.addWalletTransactions(userWallet.getCurrency(), userWalletTransactionInfo);
                    });

        }

        return userWalletTransactionsResponse;
    }

}
