package com.cryptocheckers.controller;

import com.cryptocheckers.entity.BettingRoom;
import com.cryptocheckers.entity.Match;
import com.cryptocheckers.entity.User;
import com.cryptocheckers.entity.Wallet;
import com.cryptocheckers.event.MatchEndedEvent;
import com.cryptocheckers.repository.MatchRepository;
import com.cryptocheckers.repository.UserRepository;
import com.cryptocheckers.request.CreateNewMatchRequest;
import com.cryptocheckers.request.MatchTerminatedRequest;
import com.cryptocheckers.response.CanUserPlayMatch;
import com.cryptocheckers.response.CreateNewMatchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@RestController
public class MatchController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MatchRepository matchRepository;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @PostMapping("/newMatch")
    @ResponseBody()
    public CreateNewMatchResponse createNewMatch(HttpServletRequest request,  @RequestBody @Valid CreateNewMatchRequest createNewMatchRequest) {

        /*if (request.getRemoteAddr() != "game server ip here") {
            // throw or return
            //@todo: finish this
        }*/

        User player1 = this.userRepository.findOne(Long.valueOf(createNewMatchRequest.getPlayer1()));
        User player2 = this.userRepository.findOne(Long.valueOf(createNewMatchRequest.getPlayer2()));

        if (!player1.getBettingRoom().equals(player2.getBettingRoom())) {
            //@todo: throw error, this should not happen
        }

        Match match = new Match();
        match.setBettingRoom(player1.getBettingRoom());
        match.setPlayer1(player1);
        match.setPlayer2(player2);

        match = this.matchRepository.save(match);

        return new CreateNewMatchResponse(match.getId());
    }

    @PostMapping("/matchTerminated/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void matchTerminated(HttpServletRequest request, @PathVariable("id") Long matchId, @RequestBody @Valid MatchTerminatedRequest matchTerminatedRequest) {

        /*if (request.getRemoteAddr() != "game server ip here") {
            // throw or return
            //@todo: finish this
        }*/


        Match match = this.matchRepository.findOne(matchId);

        if (Match.MatchStatus.FINISHED.equals(match.getStatus())) {
            return;
        }

        Long winnerId = matchTerminatedRequest.getWinner();
        Long looserId = matchTerminatedRequest.getLooser();

        if (winnerId == null && looserId == null) {
            match.setTie(true);

        } else {
            User winner = this.userRepository.findOne(winnerId);
            User looser = this.userRepository.findOne(looserId);

            match.setWinner(winner);
            match.setLooser(looser);
        }

        match.setStatus(Match.MatchStatus.FINISHED);
        this.matchRepository.save(match);
        this.applicationEventPublisher.publishEvent(new MatchEndedEvent(this, match));
    }

    @PostMapping("/canPlay")
    @ResponseBody()
    public CanUserPlayMatch canUserPlay(HttpServletRequest request) {
        Optional<User> userOptional = this.userRepository.findByUsername(request.getUserPrincipal().getName());

        //@todo: Add reason, for example, no funds, server being updated, user banned...
        if (userOptional.isPresent()) {
            return new CanUserPlayMatch(false);
        }

        User user = userOptional.get();
        BettingRoom bettingRoom = user.getBettingRoom();

        Wallet userWallet = user.getWallets()
                .stream()
                .filter(wallet -> wallet.getCurrency().equals(bettingRoom.getCurrency()))
                .findFirst()
                .get();

        boolean hasEnoughFunds = userWallet.getAmount().compareTo(bettingRoom.getAmount()) >= 0;
        if (!hasEnoughFunds) {
            return new CanUserPlayMatch(hasEnoughFunds, "You don't have enough funds to play.");
        }

        return new CanUserPlayMatch(true);
    }
}
