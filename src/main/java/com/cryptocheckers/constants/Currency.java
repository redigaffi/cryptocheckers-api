package com.cryptocheckers.constants;

import java.util.HashMap;

public enum Currency {
    BTC("BTC");

    private String type;
    private static final HashMap<String, String> fullCurrencyMappings = new HashMap<>() {
        {
            put("Bitcoin", BTC.getType());
        }
    };

    Currency(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static Currency fromFullCurrencyName(String currency) {
        return Currency.valueOf(Currency.fullCurrencyMappings.get(currency));
    }
}
