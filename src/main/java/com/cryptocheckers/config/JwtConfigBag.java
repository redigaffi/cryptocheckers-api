package com.cryptocheckers.config;


public class JwtConfigBag {

    public static final String HEADER_AUTHORIZATION_KEY = "Authorization";
    public static final String TOKEN_BEARER_PREFIX = "Bearer ";
    public static final String ISSUER_INFO = "Cryptocheckers";
    public static final String SECRET_KEY = "1234";
    public static final long TOKEN_EXPIRATION_KEY = 864_000_000;
}
