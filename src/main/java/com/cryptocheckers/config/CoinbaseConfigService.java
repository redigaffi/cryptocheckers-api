package com.cryptocheckers.config;

import coinbase.api.v2.service.CoinbaseAccountService;
import coinbase.api.v2.service.CoinbaseAddressService;
import coinbase.api.v2.service.CoinbaseNotificationService;
import coinbase.api.v2.service.CoinbaseTransactionService;
import coinbase.api.v2.service.auth.CoinbaseAuthenticationApiKey;
import coinbase.api.v2.service.auth.CoinbaseAuthenticationBearer;
import coinbase.api.v2.service.auth.ICoinbaseTimestampProvider;
import coinbase.api.v2.service.data.CoinbasePricesService;
import coinbase.api.v2.service.data.CoinbaseTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class CoinbaseConfigService {

    @Value("${coinbase.key}")
    private String apiKey;

    @Value("${coinbase.secret}")
    private String apiSecret;


    @Bean
    public CoinbaseAuthenticationBearer coinbaseAuthenticationBearer() {
        return new CoinbaseAuthenticationApiKey(this.apiKey, this.apiSecret);
    }

    @Bean
    public ICoinbaseTimestampProvider coinbaseTimeService() {
        return new CoinbaseTimeService();
    }

    @Bean
    @Autowired
    public CoinbaseAddressService coinbaseAddressService(ICoinbaseTimestampProvider timestampProvider) {
        return new CoinbaseAddressService(timestampProvider);
    }

    @Bean
    @Autowired
    public CoinbaseTransactionService coinbaseTransactionService(ICoinbaseTimestampProvider timestampProvider) {
        return new CoinbaseTransactionService(timestampProvider);
    }

    @Bean
    @Autowired
    public CoinbaseAccountService coinbaseAccountService(ICoinbaseTimestampProvider timestampProvider) {
        return new CoinbaseAccountService(timestampProvider);
    }

    @Bean
    public CoinbasePricesService coinbaseExchangeRatesService() {
        return new CoinbasePricesService();
    }

    @Autowired
    @Bean
    public CoinbaseNotificationService coinbaseNotificationService(ICoinbaseTimestampProvider timestampProvider) {
        return new CoinbaseNotificationService(timestampProvider);
    }

}
