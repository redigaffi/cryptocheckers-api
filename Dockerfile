FROM maven:3-jdk-12 AS builder
COPY . /usr/src/myapp
WORKDIR /usr/src/myapp
RUN mvn package

FROM openjdk:12
COPY --from=builder /usr/src/myapp/target/cryptocheckers.jar .
CMD ["java", "-Xmx200m", "-jar", "./cryptocheckers.jar"]